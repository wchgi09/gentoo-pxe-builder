#!/bin/bash

# PURPOSE:
#	Retrieve Gentoo install iso, pull kernel and initramfs
#   adjusting initramfs to support network boot install, adding
#   addtional commands to run at launch.
#
# NOTES:
#	Forked from original at https://github.com/ultrabug/gentoo-pxe-builder
#	Modified to support iso's circa May 2015
# TODO:
#	copy to tftp server
#	update pxeboot menu

SCRIPTNAME=$(basename $0)

error_exit()
{
	#-----------------------------------------------------
	# Exit on Error Function
	# Accept argument: string describing error encountered
	#-----------------------------------------------------

	echo "${SCRIPTNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}

copy_results_tftp()
{
	#----------------------------------------------
	# Copy results function
	# Put kernel and initramfs to tftp share on NAS
	#----------------------------------------------
	DESTINATION="/mnt/nas/pxe/gentoo_amd64_may_2015/"
	cp ${1} ${DESTINATION}
	cp ${2} ${DESTINATION}
}

# defaults
ARCH=${ARCH:-amd64}
MIRROR=${MIRROR:-http://distfiles.gentoo.org}
SSH_KEY_PATH=${SSH_KEY_PATH:-~/.ssh/id_rsa.pub}

mkdir -p iso
pushd iso
	#-------------------------------
	# set some key working variables
	#-------------------------------
	base_url="${MIRROR}/releases/${ARCH}/autobuilds"
	latest_iso=$(curl "${base_url}/latest-iso.txt" 2>/dev/null | grep -v '#')
	path_only=$(echo $latest_iso|cut -d ' ' -f1)
	iso=$(basename "${path_only}")

	wget -nc "${base_url}/${path_only}" || error_exit "$LINENO: Could not download iso"
	wget -nc "${base_url}/${path_only}.DIGESTS.asc" || error_exit "$LINENO: Could not download digests"
	wget -nc "${base_url}/${path_only}.CONTENTS" || error_exit "$LINENO: Could not download contents"
	#-----------------------
	# validate retrieved iso
	#-----------------------
	sha512_digests=$(grep -A1 SHA512 "${iso}.DIGESTS.asc" | grep -v '^--')
	gpg --verify "${iso}.DIGESTS.asc" || error_exit "$LINENO: Insecure digests"
	echo "${sha512_digests}" | sha512sum -c || error_exit "$LINENO: Checksum validation failed"
popd

# mount iso via loopback
mkdir mnt
sudo mount -o loop iso/${iso} mnt/

# get kernel from mounted iso
cp mnt/isolinux/gentoo .

# mount squashed filesystem from mounted iso
mkdir squashmnt squash
sudo mount -t squashfs -o loop mnt/image.squashfs squashmnt/
# Copy out files from mounted squashed filesystem
sudo cp -a squashmnt/* squash/ || error_exit "$LINENO: Could not copy out squashed files"
# umount squashed filesystem and remove mount point
sudo umount squashmnt/ && rmdir squashmnt

#-------------------------------------------
# Apply startup customizations to filesystem
#-------------------------------------------

# copy setup.start script and make executable
sudo cp files/setup.start squash/etc/local.d/
sudo chmod +x squash/etc/local.d/setup.start

sudo mkdir -p squash/root/.ssh
if [ -f "${SSH_KEY_PATH}" ]; then
	sudo cp "${SSH_KEY_PATH}" squash/root/.ssh/authorized_keys
	sudo chmod 600 squash/root/.ssh/authorized_keys
fi

# create squashed filesystem with customizations and remove source directory
sudo mksquashfs squash/ image.squashfs
sudo rm -rf squash

#-------------------------------------
# Build new network bootable initramfs
#-------------------------------------
mkdir igz
pushd igz
	# uncompress initramfs from iso
	xzcat ../mnt/isolinux/gentoo.igz | sudo cpio -idv &>/dev/null
	# apply patch to overcome real root detecton to allow network boot
	patch < ../files/init.livecd.patch
	# create cd mount point and file system in intramfs
	sudo mkdir -p mnt/cdrom
	sudo mv ../image.squashfs mnt/cdrom/
	# recompress modified initramfs
	find . -print | cpio -o -H newc | gzip -9 -c - > ../gentoo.igz
popd

# remove initramfs source directory
sudo rm -rf igz

# unmount iso and remove mount point
sudo umount mnt && rmdir mnt

clear
echo "All done:"
echo "---------"
echo "  - PXE kernel file : gentoo"
echo "  - PXE initramfs file : gentoo.igz"

copy_results_tftp gentoo  gentoo.igz
